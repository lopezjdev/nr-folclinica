package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;
    private final PacienteMapper pacienteMapper;

    public PacienteController(PacienteService pacienteService, PacienteMapper pacienteMapper) {
        this.pacienteService = pacienteService;
        this.pacienteMapper = pacienteMapper;
    }


    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Page<PacienteDto>> listarUnoPorPage(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.listarUnoPorPage(id))
                ;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<PacienteDto>> listarPorPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity.ok(pacienteService.listarPorPage(pageNumber, pageSize, orderField));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity) {
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idpaciente}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "idpaciente") int id,
                                                    @RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(
                pacienteService.editar(id, dto)
        );
    }


    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idpaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idpaciente") int id){
        return ResponseEntity.ok(
                pacienteService.eliminar(id)
        );
    }
}
