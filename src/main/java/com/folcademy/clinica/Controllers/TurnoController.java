package com.folcademy.clinica.Controllers;



import com.folcademy.clinica.Model.Dto.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;


@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }


    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<Page<TurnoDto>> listarUnoPorPage(@PathVariable(name = "idTurno") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.listarUnoPorPage(id))
                ;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/page")
    public ResponseEntity<Page<TurnoDto>> listarPorPage(
            @RequestParam(name = "pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "apellido") String orderField
    ) {
        return ResponseEntity.ok(turnoService.listarPorPage(pageNumber, pageSize, orderField));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity) {
        return ResponseEntity
                .ok(turnoService.agregar(entity));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idturno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idturno") Integer id,
                                           @RequestBody @Validated TurnoDto dto){
        return ResponseEntity.ok(
                turnoService.editar(id, dto)
        );
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idturno}/{fecha}")
    public ResponseEntity<Boolean> editarFecha(@PathVariable(name = "idturno") int id,
                                                @PathVariable(name = "fecha") LocalDate fecha) {
        return ResponseEntity.ok(
                turnoService.editarFecha(id, fecha)
        );
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idturno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name="idturno") Integer id){
        return ResponseEntity.ok(
                turnoService.eliminar(id)
        );
    }
}
