package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicoRepository extends JpaRepository<Medico, Integer> {
    Page<Medico> findAll(Pageable pageable);
    Page<Medico> findById(Integer id, Pageable pageable);


}
