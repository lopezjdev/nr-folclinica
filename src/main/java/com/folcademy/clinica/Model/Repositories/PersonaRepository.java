package com.folcademy.clinica.Model.Repositories;


import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends PagingAndSortingRepository<Persona, Integer> {
    //extends JpaRepository <Persona, Integer>
}
