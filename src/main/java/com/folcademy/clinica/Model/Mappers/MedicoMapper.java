package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Dto.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {

    private final PersonaMapper personaMapper = new PersonaMapper();

    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                personaMapper.entityToDto(entity.getPersona()),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoDto());

    }

    public Medico dtoToEntity(MedicoDto dto) {
        Medico entity = new Medico();
        entity.setPersona(personaMapper.dtoToEntity(dto.getPersona()));
        entity.setDni(personaMapper.dtoToEntity(dto.getPersona()).getDni());
        entity.setId(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        return entity;
    }
}


