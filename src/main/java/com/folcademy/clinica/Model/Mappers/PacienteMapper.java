package com.folcademy.clinica.Model.Mappers;


import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Dto.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {

    private final PersonaMapper personaMapper = new PersonaMapper();

    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                personaMapper.entityToDto(entity.getPersona()),
                                ent.getDireccion()
                             )
                        )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setPersona(personaMapper.dtoToEntity(dto.getPersona()));
        entity.setDni(personaMapper.dtoToEntity(dto.getPersona()).getDni());
        entity.setId(dto.getId());
        entity.setDireccion(dto.getDireccion());
        return entity;
    }
}