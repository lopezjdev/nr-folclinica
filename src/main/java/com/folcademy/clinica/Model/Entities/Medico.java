package com.folcademy.clinica.Model.Entities;


import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor

public class Medico
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;
    @Column(name = "profesion", columnDefinition = "VARCHAR")
    public String profesion;
    @Column(name = "consulta", columnDefinition = "INT(10) UNSIGNED")
    public int consulta = 0;
    @Column(name = "dni", columnDefinition = "INT(11)")
    public Integer dni;


    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni", referencedColumnName = "dni", insertable = false, updatable = false)
    private Persona persona;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;
        return id != null && Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}