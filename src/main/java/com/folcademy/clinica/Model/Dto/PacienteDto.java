package com.folcademy.clinica.Model.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PacienteDto {

    Integer id;
    PersonaDto persona;
    String direccion = "";

}
