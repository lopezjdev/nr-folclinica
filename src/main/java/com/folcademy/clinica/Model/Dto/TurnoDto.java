package com.folcademy.clinica.Model.Dto;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {

    Integer idturno;

    LocalDate fecha;

    LocalTime hora;

    Boolean atendido;

    Integer idpaciente;

    Integer idmedico;

}
