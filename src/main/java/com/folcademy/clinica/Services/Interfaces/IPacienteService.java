package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Dto.PacienteDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPacienteService {

    Page<PacienteDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField);
    Page<PacienteDto> listarUnoPorPage(Integer id);
    PacienteDto agregar(PacienteDto entity);
    PacienteDto editar(Integer idpaciente, PacienteDto dto);
    boolean eliminar(Integer id);


}
