package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Dto.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ITurnoService {

    Page<TurnoDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField);
    Page<TurnoDto> listarUnoPorPage(Integer id);

}
