package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dto.MedicoDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IMedicoService {

    Page<MedicoDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField);
    Page<MedicoDto> listarUnoPorPage(Integer id);
    MedicoDto agregar(MedicoDto entity);
    MedicoDto editar(Integer idMedico, MedicoDto dto);
    boolean editarConsulta(Integer idMedico, Integer consulta);
    boolean eliminar(Integer id);

}
