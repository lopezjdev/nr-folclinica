package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;

    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public Page<MedicoDto> listarUnoPorPage(Integer id) {
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Error. La ID solicitada no coincide con ningún médico en nuestro sistema.");
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return medicoRepository.findById(id, pageable).map(medicoMapper::entityToDto);
    }

    public Page<MedicoDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }


    public MedicoDto agregar(MedicoDto entity){
        entity.setId(null);
        if(entity.getConsulta()<0)
            throw new BadRequestException("Error. La consulta no puede ser menor que 0");
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }
    
     public MedicoDto editar(Integer idMedico, MedicoDto dto) {
        if (!medicoRepository.existsById(idMedico))
            throw new NotFoundException("Error. No existe el médico con la ID indicada.");
        dto.setId(idMedico);
        return medicoMapper.entityToDto(
                        medicoRepository.save(
                                medicoMapper.dtoToEntity(
                                        dto
                        )
                )
        );

     }

     public boolean editarConsulta(Integer idMedico, Integer consulta){
        if (medicoRepository.existsById(idMedico)) {
            Medico entity = medicoRepository.findById(idMedico).orElse(new Medico());
            entity.setConsulta(consulta);
            medicoRepository.save(entity);
            return true;
        }
        return false;
     }

     public boolean eliminar(Integer id){
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("Error. No existe o no se encuentra el medico con la ID indicada.");
        medicoRepository.deleteById(id);
        return true;
     }
}