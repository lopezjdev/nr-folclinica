package com.folcademy.clinica.Services;

import java.sql.Date;
import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Dto.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {

        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }


    public Page<TurnoDto> listarUnoPorPage(Integer id){
        if(!turnoRepository.existsById(id))
            throw new NotFoundException("No existe el turno solicitado");
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return turnoRepository.findById(id, pageable).map(turnoMapper::entityToDto);
    }

    public Page<TurnoDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    public TurnoDto agregar(TurnoDto entity){
        if(entity.getIdturno()<0)
            throw new BadRequestException("Error. El turno no puede ser menor que 0");
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }

    public TurnoDto editar(Integer idTurno, TurnoDto dto){
        if (!turnoRepository.existsById(idTurno))
            throw new NotFoundException("Error. El turno solicitado no existe.");
        dto.setIdturno(idTurno);
        return turnoMapper.entityToDto(
                turnoRepository.save(
                        turnoMapper.dtoToEntity(
                                dto
                        )
                )
        );
    }

    public boolean editarFecha(Integer idTurno, LocalDate fecha){
        if(turnoRepository.existsById(idTurno)){
            Turno entity = turnoRepository.findById(idTurno).orElse(new Turno());
            entity.setFecha(fecha);
            turnoRepository.save(entity);
            return true;
        }
        return false;
    }

    public boolean eliminar(Integer idTurno){
        if(!turnoRepository.existsById(idTurno))
            throw new NotFoundException("Error. No existe el turno solicitado. Intente nuevamente.");
        turnoRepository.deleteById(idTurno);
        return true;
    }
}
