package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dto.PacienteDto;
import com.folcademy.clinica.Model.Dto.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;

    private final PacienteMapper pacienteMapper;

    private final PersonaMapper personaMapper;

    private final PersonaRepository personaRepository;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaMapper personaMapper, PersonaRepository personaRepository) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
    }


    public Page<PacienteDto> listarPorPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }

    public Page<PacienteDto> listarUnoPorPage(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Error. La ID solicitada no coincide con ningún paciente");
        Pageable pageable = PageRequest.of(0,1,Sort.by("id"));
        return pacienteRepository.findById(id, pageable).map(pacienteMapper::entityToDto);
    }

    public PacienteDto agregar(PacienteDto entity){

        if(entity.getPersona().getDni() < 0)
            throw new BadRequestException("Error. Ingrese el DNI del paciente correctamente.");
        entity.setId(null);
        personaRepository.save(personaMapper.dtoToEntity(entity.getPersona()));
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }

    public PacienteDto editar(Integer idPaciente, PacienteDto dto){
        if (!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("Error. La ID solicitada no está asociada a ningún paciente");
        dto.setId(idPaciente);
        personaRepository.save(personaMapper.dtoToEntity(dto.getPersona()));
        return pacienteMapper.entityToDto(
                pacienteRepository.save(
                        pacienteMapper.dtoToEntity(
                                dto
                        )
                )
        );
    }


    public boolean eliminar(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("Error. La ID solicitada no se encuentra en nuestra base de datos.");
        pacienteRepository.deleteById(id);
        return true;
    }

}
